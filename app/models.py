# -*- coding: utf-8 -*-

from django.db import models

class Messages(models.Model):
	text = models.TextField('Сообщение', null=False, blank=False)
	datetime = models.DateTimeField('Время добавления', auto_now_add=True)
	is_hidden = models.BooleanField('Скрыто', default=False)

	def __unicode__(self):
		return self.text
