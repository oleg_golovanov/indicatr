# -*- coding: utf-8 -*-

from django import forms

class AddMessageForm(forms.Form):
    message = forms.CharField(max_length=1000)
