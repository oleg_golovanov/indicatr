# -*- coding: utf-8 -*-

from django.http import HttpResponse, HttpResponseServerError, HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db import DatabaseError
from json import dumps

from models import Messages
from forms import AddMessageForm

def messages(request):
	if request.method == 'POST':
		try:
			Messages.objects.create(text = request.POST['message'])
			return HttpResponseRedirect('/thanks/')
		except DatabaseError:
			return HttpResponseServerError('500 :(')
	if request.method == 'GET':
		try:
			objects = Messages.objects.filter(is_hidden=False).order_by('-id').values('id', 'text')
			data = list(objects)
			return HttpResponse(dumps(data, sort_keys=True))
		except DatabaseError:
			return HttpResponseServerError('500 :(')
	else:
		return HttpResponse('Приложение поддерживает только GET и POST запросы.\n')

def add_message(request):
	form = AddMessageForm()
	return render(request, 'add_message.html', {'form': form})

def mark_as_read(request, message_id):
	try:
		message = Messages.objects.get(pk=message_id)
		message.is_hidden = True
		message.save()
		return HttpResponse('OK')
	except DatabaseError:
		return HttpResponseServerError('500 :(')
	except Messages.DoesNotExist:
		return HttpResponseNotFound('404 :(')

def thanks(request):
	return render(request, 'thanks.html')

def index(request):
	return render(request, 'index.html')
