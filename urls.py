from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'indicatr.views.home', name='home'),
    # url(r'^indicatr/', include('indicatr.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
	url(r'^add_message/$', 'indicatr.app.views.add_message'),
	url(r'^messages/$', 'indicatr.app.views.messages'),
	url(r'^messages/(\d+)/mark_as_read/$', 'indicatr.app.views.mark_as_read'),
    url(r'^thanks/$', 'indicatr.app.views.thanks'),
	url(r'^$', 'indicatr.app.views.index'),
)