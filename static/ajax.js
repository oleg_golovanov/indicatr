$(document).ready(function() {

	current = new Array();

	var array_diff = function(a1, a2) {
		var k = 0;
		tmp = a1.slice();
		for (var i = 0; i < a1.length; i++) {
			for (var j = 0; j < a2.length; j++) {
				if (a1[i].id == a2[j].id) {
					tmp.splice(i-k, 1);
					k++;
				};
			};
		};
		return tmp;
	}

	var render_list = function(){
		$.getJSON("/messages/", function(data) {
			json = new Array();
			$.each(data, function() {
				json.push({id: this.id, text: this.text});
			});

			if (current.length == 0) {
				for (var i = 0; i < json.length; i++) {
					current.push({status: 1, id: json[i].id, text: json[i].text});
				};
			}
			else{
				add_items = array_diff(json, current);
				rem_items = array_diff(current, json);
				
				for (var i = add_items.length -1; i >= 0; i--) {
					current.unshift({status: 1, id: add_items[i].id, text: add_items[i].text});
				};

				for (var i = 0; i < rem_items.length; i++) {
					for (var j = 0; j < current.length; j++) {
						if (rem_items[i].id == current[j].id) {
							current[j].status = -1;
						};
					};
				};
			};

			for (var i = current.length - 1; i >= 0; i--) {
				if (current[i].status == 1) {
					$('#messages').prepend('<li id="item-' + current[i].id + '">' + current[i].text + '<span>X</span></li>');
					item = $('#item-' + current[i].id);
					item.hide().fadeIn();
					current[i].status = 0;
				}
				else if (current[i].status == -1) {
					item = $('#item-' + current[i].id);
					item.fadeOut();
					current.splice(i, 1);
				};
			};
		});
	}

	$('#messages').delegate(
		'span', 
		'click',
		function() {
			var item = $(this).parent();
			var id = item.attr('id').split('-')[1];
			$.get("/messages/" + id + "/mark_as_read/", function() {
				item.fadeOut();
			}); 
		}
	);

	render_list();
	setInterval(render_list, 5000);

});
